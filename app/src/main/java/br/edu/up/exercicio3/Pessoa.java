package br.edu.up.exercicio3;

import java.io.Serializable;

/**
 * Universidade Positivo
 * Bacharelado em Sistemas de Informação
 * Criado por Geucimar Brilhador
 * 27/08/2015
 */
public class Pessoa implements Serializable {


    private final String nome;
    private final String cpf;
    private final String celular;
    private final String dataNascimento;

    public Pessoa(String nome, String cpf, String celular, String dataNascimento){
        this.nome = nome;
        this.cpf = cpf;
        this.celular = celular;
        this.dataNascimento = dataNascimento;
    }

    public String getNome() {
        return nome;
    }

    public String getCpf() {
        return cpf;
    }

    public String getCelular() {
        return celular;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public String getIdade(){
        return Util.getIdade(dataNascimento);
    }
}