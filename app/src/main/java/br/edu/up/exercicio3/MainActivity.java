package br.edu.up.exercicio3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void jogar(View view) {
        EditText txtNome = (EditText) findViewById(R.id.txtNome);
        EditText txtCPF = (EditText) findViewById(R.id.txtCPF);
        EditText txtCelular = (EditText) findViewById(R.id.txtCelular);
        EditText txtDataNascimento = (EditText) findViewById(R.id.txtDataNascimento);

        String nome = txtNome.getText().toString();
        String cpf = txtCPF.getText().toString();
        String celular = txtCelular.getText().toString();
        String dataNascimento = txtDataNascimento.getText().toString();

        if (Util.isCPFValido(cpf) &&
                Util.isDataValida(dataNascimento) &&
                Util.isMaiorDeIdade(dataNascimento)){
            Pessoa pessoa = new Pessoa(nome, cpf, celular, dataNascimento);
            Intent in = new Intent(this, DadosActivity.class);
            in.putExtra("pessoa", pessoa);
            startActivity(in);
        } else if(!Util.isCPFValido(cpf)){
            Toast.makeText(this, "O CPF informado é inválido!", Toast.LENGTH_LONG).show();
        } else if(!Util.isDataValida(dataNascimento)){
            Toast.makeText(this, "A data informada é inválida!", Toast.LENGTH_LONG).show();
        } else if(!Util.isMaiorDeIdade(dataNascimento)){
            Toast.makeText(this, "A pessoa informada é menor de idade!", Toast.LENGTH_LONG).show();
        }
    }
}