package br.edu.up.exercicio3;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.InputMismatchException;

/**
 * Universidade Positivo
 * Bacharelado em Sistemas de Informação
 * Criado por Geucimar Brilhador
 * 14/10/2013
 *
 */
public class Util {


    public static boolean isCPFValido(String CPF) {

        CPF = limpar(CPF);

        if (CPF.equals("00000000000") ||
                CPF.equals("11111111111") ||
                CPF.equals("22222222222") ||
                CPF.equals("33333333333") ||
                CPF.equals("44444444444") ||
                CPF.equals("55555555555") ||
                CPF.equals("66666666666") ||
                CPF.equals("77777777777") ||
                CPF.equals("88888888888") ||
                CPF.equals("99999999999") ||
                (CPF.length() != 11)) {
            return (false);
        }

        char digito10, digito11;
        int soma, resultado, numero, peso;

        try {
            soma = 0;
            peso = 10;
            for (int i=0; i<9; i++) {
                numero = (int)(CPF.charAt(i) - 48);
                soma = soma + (numero * peso);
                peso = peso - 1;
            }

            resultado = 11 - (soma % 11);
            if ((resultado == 10) || (resultado == 11))
                digito10 = '0';
            else digito10 = (char)(resultado + 48);

            soma = 0;
            peso = 11;
            for(int i=0; i<10; i++) {
                numero = (int)(CPF.charAt(i) - 48);
                soma = soma + (numero * peso);
                peso = peso - 1;
            }

            resultado = 11 - (soma % 11);
            if ((resultado == 10) || (resultado == 11))
                digito11 = '0';
            else digito11 = (char)(resultado + 48);

            if ((digito10 == CPF.charAt(9)) && (digito11 == CPF.charAt(10)))
                return(true);
            else return(false);

        } catch (InputMismatchException erro) {
            return(false);
        }
    }

    public static boolean isMaiorDeIdade(String dataNascimento) {

        boolean isMaiorDeIdade = false;
        try {
            dataNascimento = limpar(dataNascimento);
            DateFormat df = new SimpleDateFormat("ddMMyyyy");
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(new Date());
            calendar.add(Calendar.YEAR, -18);

            Date dataLimite = calendar.getTime();
            Date dataInformada = df.parse(dataNascimento);

            if (dataInformada.compareTo(dataLimite) < 0){
                isMaiorDeIdade = true;
            }

        } catch (ParseException e) {
        }
        return isMaiorDeIdade;
    }

    public static String getIdade(String dataNascimento) {
        dataNascimento = limpar(dataNascimento);
        DateFormat df = new SimpleDateFormat("ddMMyyyy");
        String idade = "0";
        try {

            Date dataInformada = df.parse(dataNascimento);
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(dataInformada);
            int anoNascimento = calendar.get(Calendar.YEAR);

            Calendar calendarHoje = new GregorianCalendar();
            calendar.setTime(new Date());
            int anoAtual = calendar.get(Calendar.YEAR);

            idade = String.valueOf(anoAtual - anoNascimento);

        } catch (ParseException e) {
        }
        return idade;
    }

    public static boolean isDataValida(String data) {

        boolean isDataValida = false;
        try {
            data = limpar(data);
            DateFormat df = new SimpleDateFormat("ddMMyyyy");
            df.setLenient(false);
            df.parse(data);
            isDataValida = true;
        } catch (ParseException e) {
        }
        return isDataValida;
    }

    public static String formatarCPF(String CPF) {
        return(CPF.substring(0, 3) + "." + CPF.substring(3, 6) + "." +
                CPF.substring(6, 9) + "-" + CPF.substring(9, 11));
    }

    public static String formatarTelefone(String numero) {
        if (numero != null) {

            if (numero.length() == 8) {
                return numero.substring(0, 4) + "-"
                        + numero.substring(4, 8);
            }

            if (numero.length() == 10) {
                return "(" + numero.substring(0, 2) + ") "
                        + numero.substring(2, 6) + "-"
                        + numero.substring(6, 10);
            }
        }
        return numero;
    }

    public static String limpar(String str) {
        return str.replaceAll("/[^0-9]+/g", "");
    }
}